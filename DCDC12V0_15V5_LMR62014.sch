EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 3450 2700 0    50   Input ~ 0
Vin12V0
Text HLabel 3450 3750 0    50   Input ~ 0
GND
Text HLabel 5850 2700 2    50   Output ~ 0
Vout15V5
$Comp
L Regulator_Switching:LMR62014XMF U?
U 1 1 5E251013
P 4350 3150
AR Path="/5E251013" Ref="U?"  Part="1" 
AR Path="/5E23136E/5E251013" Ref="U2"  Part="1" 
F 0 "U2" H 4350 3517 50  0000 C CNN
F 1 "LMR62014XMF" H 4350 3426 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 4400 2900 50  0001 L CIN
F 3 "http://www.ti.com/lit/ds/symlink/lmr62014.pdf" H 4350 3250 50  0001 C CNN
F 4 "Texas Instruments" H 4350 3150 50  0001 C CNN "Manufacturer"
F 5 "LMR62014" H 4350 3150 50  0001 C CNN "Part Number"
	1    4350 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 3050 4050 3250
Connection ~ 4050 3050
$Comp
L Device:L L?
U 1 1 5E25101E
P 4350 2700
AR Path="/5E25101E" Ref="L?"  Part="1" 
AR Path="/5E23136E/5E25101E" Ref="L1"  Part="1" 
F 0 "L1" V 4540 2700 50  0000 C CNN
F 1 "10u" V 4449 2700 50  0000 C CNN
F 2 "Inductor_SMD:L_Taiyo-Yuden_NR-60xx" H 4350 2700 50  0001 C CNN
F 3 "" H 4350 2700 50  0001 C CNN
F 4 "Taiyo Yuden" V 4350 2700 50  0001 C CNN "Manufacturer"
F 5 "NR6045T100M" V 4350 2700 50  0001 C CNN "Part Number"
	1    4350 2700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4200 2700 3900 2700
Wire Wire Line
	3900 2700 3900 3050
Connection ~ 3900 3050
Wire Wire Line
	3900 3050 4050 3050
Wire Wire Line
	4500 2700 4700 2700
Wire Wire Line
	4700 2700 4700 3050
Wire Wire Line
	4700 3050 4650 3050
$Comp
L Device:C C?
U 1 1 5E25102D
P 3900 3300
AR Path="/5E25102D" Ref="C?"  Part="1" 
AR Path="/5E23136E/5E25102D" Ref="C4"  Part="1" 
F 0 "C4" H 4015 3346 50  0000 L CNN
F 1 "2.2u" H 4015 3255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3938 3150 50  0001 C CNN
F 3 "" H 3900 3300 50  0001 C CNN
F 4 "Murata" H 3900 3300 50  0001 C CNN "Manufacturer"
F 5 "GRM21BR71C225KA12L" H 3900 3300 50  0001 C CNN "Part Number"
	1    3900 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 3150 3900 3050
$Comp
L Device:R R?
U 1 1 5E25103C
P 5000 3150
AR Path="/5E25103C" Ref="R?"  Part="1" 
AR Path="/5E23136E/5E25103C" Ref="R11"  Part="1" 
F 0 "R11" V 4900 3250 50  0000 C CNN
F 1 "154k" V 4900 3050 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4930 3150 50  0001 C CNN
F 3 "" H 5000 3150 50  0001 C CNN
F 4 "Vishay" V 5000 3150 50  0001 C CNN "Manufacturer"
F 5 "CRCW0603154KFKEA" V 5000 3150 50  0001 C CNN "Part Number"
	1    5000 3150
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5E251044
P 5000 3350
AR Path="/5E251044" Ref="C?"  Part="1" 
AR Path="/5E23136E/5E251044" Ref="C5"  Part="1" 
F 0 "C5" V 4950 3450 50  0000 C CNN
F 1 "220p" V 4950 3250 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5038 3200 50  0001 C CNN
F 3 "" H 5000 3350 50  0001 C CNN
F 4 "Kemet" V 5000 3350 50  0001 C CNN "Manufacturer"
F 5 "C0603C221J5GACTU" V 5000 3350 50  0001 C CNN "Part Number"
	1    5000 3350
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5E25104C
P 5250 3550
AR Path="/5E25104C" Ref="C?"  Part="1" 
AR Path="/5E23136E/5E25104C" Ref="C6"  Part="1" 
F 0 "C6" H 5365 3596 50  0000 L CNN
F 1 "4.7u" H 5365 3505 50  0000 L CNN
F 2 "Capacitor_SMD:C_1210_3225Metric" H 5288 3400 50  0001 C CNN
F 3 "" H 5250 3550 50  0001 C CNN
F 4 "Murata" H 5250 3550 50  0001 C CNN "Manufacturer"
F 5 "GRM32ER71H475KA88L" H 5250 3550 50  0001 C CNN "Part Number"
	1    5250 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E251054
P 5550 3550
AR Path="/5E251054" Ref="C?"  Part="1" 
AR Path="/5E23136E/5E251054" Ref="C7"  Part="1" 
F 0 "C7" H 5665 3596 50  0000 L CNN
F 1 "0.022u" H 5665 3505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5588 3400 50  0001 C CNN
F 3 "" H 5550 3550 50  0001 C CNN
F 4 "Murata" H 5550 3550 50  0001 C CNN "Manufacturer"
F 5 "GRM188R72A223KAC4D" H 5550 3550 50  0001 C CNN "Part Number"
	1    5550 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5E25105C
P 4750 3550
AR Path="/5E25105C" Ref="R?"  Part="1" 
AR Path="/5E23136E/5E25105C" Ref="R10"  Part="1" 
F 0 "R10" H 4680 3504 50  0000 R CNN
F 1 "13.3k" H 4680 3595 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4680 3550 50  0001 C CNN
F 3 "" H 4750 3550 50  0001 C CNN
F 4 "Vishay" H 4750 3550 50  0001 C CNN "Manufacturer"
F 5 "CRCW060313K3FKEA" H 4750 3550 50  0001 C CNN "Part Number"
	1    4750 3550
	-1   0    0    1   
$EndComp
Wire Wire Line
	4650 3250 4750 3250
Wire Wire Line
	4850 3250 4850 3150
Wire Wire Line
	4850 3350 4850 3250
Connection ~ 4850 3250
Wire Wire Line
	4750 3400 4750 3250
Connection ~ 4750 3250
Wire Wire Line
	4750 3250 4850 3250
Wire Wire Line
	5150 3150 5150 3250
Wire Wire Line
	5250 3400 5250 3250
Wire Wire Line
	5250 3250 5150 3250
Connection ~ 5150 3250
Wire Wire Line
	5150 3250 5150 3350
Wire Wire Line
	5250 3250 5550 3250
Wire Wire Line
	5550 3250 5550 3400
Connection ~ 5250 3250
$Comp
L Device:D D?
U 1 1 5E251073
P 5000 2700
AR Path="/5E251073" Ref="D?"  Part="1" 
AR Path="/5E23136E/5E251073" Ref="D8"  Part="1" 
F 0 "D8" H 5000 2484 50  0000 C CNN
F 1 "D" H 5000 2575 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123F" H 5000 2700 50  0001 C CNN
F 3 "" H 5000 2700 50  0001 C CNN
F 4 "Toshiba" H 5000 2700 50  0001 C CNN "Manufacturer"
F 5 "CRS08(TE85L,Q,M)" H 5000 2700 50  0001 C CNN "Part Number"
	1    5000 2700
	-1   0    0    1   
$EndComp
Wire Wire Line
	4700 2700 4850 2700
Connection ~ 4700 2700
Wire Wire Line
	5250 3250 5250 2700
Wire Wire Line
	5250 2700 5150 2700
Wire Wire Line
	4750 3700 4750 3750
Wire Wire Line
	3900 3450 3900 3750
Wire Wire Line
	5250 3700 5250 3750
Wire Wire Line
	5250 3750 4750 3750
Connection ~ 4750 3750
Wire Wire Line
	5250 3750 5550 3750
Wire Wire Line
	5550 3750 5550 3700
Connection ~ 5250 3750
Wire Wire Line
	5250 2700 5850 2700
Connection ~ 5250 2700
Wire Wire Line
	3450 2700 3900 2700
Connection ~ 3900 2700
Wire Wire Line
	3900 3750 4350 3750
Wire Wire Line
	4350 3450 4350 3750
Connection ~ 4350 3750
Wire Wire Line
	4350 3750 4750 3750
Wire Wire Line
	3450 3750 3900 3750
Connection ~ 3900 3750
$EndSCHEMATC
